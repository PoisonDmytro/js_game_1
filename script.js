//_______let_zone___________________________________
let username;

let live = true;
let turn = 1;
let record_name = "";
let record = 0;

let hero_heals = 10;
let hero_max_heals = 10;            // |base stats
let hero_min_attack = 1;            // |
let hero_max_attack = 3;            // |

let enemy_heals = 9; 
let enemy_max_heals = 9;            // |base stats 
let enemy_min_attack = 1;           // |
let enemy_max_attack = 2;           // |

let hero_dmg = Number;
let enemy_dmg = Number;

//_______code_zone__________________________________

game();

//_______functions_zone_____________________________
function game(){
username = prompt("Type your username", "KypoW,yn")

start_massage(username);

while(live == true){

    fight();
    end_fight();
    if(live == true){
        let start = confirm("Сontinue?")
        if(start == true){
            turn++;
            console.clear();
        }else{
            console.clear();
            live = false;
        }
    } 
}
end_massage();
restart();
}
function start_massage(username){
        massege=`hello, ${username}.\nYour way is started \n______________________________`
        console.log(massege);
        let start = confirm("You want to start ?")
        if(start == true){
            live = true;
        }else{
            live = false; 
            turn = 0;
        }
}
function end_massage(){
    console.log(`You played ${turn} turns !!!`);
    if(turn < 5){
        console.log("sad result");
    }else if(turn >= 5 && turn < 25){
        console.log("ok, well done");
    }else{
        console.log("WOW, you're great"); 
    }
    if(turn > record){
        record_name = username;
        record = turn;
        console.log("New Record !!!");
        console.log(`${record_name} : ${record}`);
    }else if(turn == record){
        console.log("So Close");
        console.log(`${record_name} : ${record}`);
    }else{
        console.log("Record");
        console.log(`${record_name} : ${record}`);
    }
}
function restart(){
    reset();
    let restart_value = confirm("Do you want to try again?");
    if(restart_value == true){
        live == true;
        game();
    }else{
        console.log("Goodbye");
    }
}
function reset(){
    hero_heals = 10;
    hero_max_heals = 10;            // |base stats
    hero_min_attack = 1;            // |
    hero_max_attack = 3;            // |

    enemy_heals = 9; 
    enemy_max_heals = 9;            // |base stats 
    enemy_min_attack = 1;           // |
    enemy_max_attack = 2;           // |    
}
function fight(){
    hero_heals = hero_max_heals;
    enemy_heals = enemy_max_heals;
    fight_info();
    console.log(`\n  Turn ${turn} \n______________________________`);
    while(true){
        if(enemy_heals > 0){
            hero_hit();
        }
        if(enemy_heals > 0){
            enemy_hit();
        }
        if(enemy_heals <= 0){
            console.log(`${username} WIN`);
            break;
        }
        if(hero_heals <= 0){
            console.log(`${username} LOSE`);
            live = false;
            break;  
        }
    }
    
}

function fight_info(){
    console.log(`${username} : ${hero_heals} hp \nEnemy : ${enemy_heals} hp \n______________________________`);
}

function end_fight(){
    let end = confirm("The fight is over, let's continue")
    if(end == true){
        console.clear();
    }
    if(live == true){
        enemy_upgrade();
        hero_upgrade();
        character_statistic()
    }
}

function hero_hit(){
    hero_dmg = Math.floor(Math.random() * ((hero_max_attack+1) - hero_min_attack)+ hero_min_attack);
    console.log(`${username} deal ${hero_dmg} damage`);
    enemy_heals = enemy_heals - hero_dmg;
    fight_info();
}

function enemy_hit(){
    enemy_dmg = Math.floor(Math.random() * ((enemy_max_attack+1) - enemy_min_attack)+ enemy_min_attack);
    console.log(`Enemy deal ${enemy_dmg} damage`);
    hero_heals = hero_heals - enemy_dmg;
    fight_info();
}

function enemy_upgrade(){
    let upgrade = Number;
    if(enemy_min_attack !== enemy_max_attack){
        upgrade = Math.floor(Math.random() * ((3+1) - 1)+1);
    }else{
        upgrade = Math.floor(Math.random() * (3 - 1)+1);
    }
    switch(upgrade){
        case 1 :
            enemy_max_heals++;
            console.log("Enemy heal is incresed \n______________________________");
            break;
        case 2 :
            enemy_max_attack++;
            console.log("Enemy max-attack is incresed \n______________________________");
            break;
        case 3 :
            enemy_min_attack++;
            console.log("Enemy min-attack is incresed \n______________________________");
            break;
    }

}

function hero_upgrade(){
    let upgrade = Number;
    upgrade = null;
    let key = true;
    while(key == true){
        if(upgrade == null){
            upgrade = prompt("Enter a number to select an upgrade \n1 - increase HP \n2 - increase max-attack \n 3 - increase min-attack")
        }
        if(upgrade == 1){
            hero_max_heals++;
            console.log("Your heal is incresed \n______________________________");
            key = false;
        }else if(upgrade == 2){
            hero_max_attack++;
            console.log("Your max-attack is incresed \n______________________________");
            key = false;
        }else if(upgrade == 3 && hero_min_attack !== hero_max_attack){
            hero_min_attack++;
            console.log("Your min-attack is incresed \n______________________________");
            key = false;
        }else{
            upgrade = prompt("YOU ENTERED INCORRECT DATA \nEnter a number to select an upgrade \n1 - increase HP \n2 - increase max-attack \n 3 - increase min-attack")
        }
    } 
}

function character_statistic(){
    console.log(`${username}   \nheals: ${hero_max_heals} \natack : ${hero_min_attack}-${hero_max_attack}\n`);
    console.log(`Enemy   \nheals: ${enemy_max_heals} \natack : ${enemy_min_attack}-${enemy_max_attack}\n______________________________`);
}